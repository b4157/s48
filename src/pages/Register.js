import React, {useState, useEffect, useContext} from 'react';
import	{Form, Button} from "react-bootstrap";
import Swal from 'sweetalert2';
import UserContext from '../UserContext';
import {Navigate, useNavigate} from 'react-router-dom';

export default function Register(){

	const {user} = useContext(UserContext);
	const navigate = useNavigate();

	// store values of input fields
	const [email, setEmail] = useState('');
	const [password1, setPassword1] = useState('');
	const [password2, setPassword2] = useState('');
	const [isActive, setIsActive] = useState(true);

	useEffect(()=>{
		if((email !=='' && password1 !== '' && password2 !=='') && (password1 === password2)){
			setIsActive(true);

		} else{
			setIsActive(false);
		}
	}, [email, password1, password2])

	
	function registerUser(e){
		e.preventDefault();


		// fetch('http://localhost:4000/users/register', {
		// 	method: 'POST',
		// 	headers: { 'Content-Type': 'application/json' },
		// 	body: JSON.stringify({
		// 		firstName: firstName,
		// 	    lastName: lastName,
		// 	    email: email,
		// 	    password: password,
		// 	    gender: sex,
		// 	    mobileNo: mobileNum
		// 	})
		// })
		// .then(response => response.json())
		// .then(data => {
		// 	console.log(data)

		// 	if (data.accessToken !== undefined){
		// 		localStorage.setItem('accessToken', data.accessToken);
		// 		localStorage.setItem('email', data.email);

		// 		setUser({
		// 			accessToken: data.accessToken})

		// 		setEmail('');
		// 		setPassword1('');
		// 		Swal.fire({
		// 		  title: 'Good job!',
		// 		  text: `${data.email} Login successful!`,
		// 		  icon:'success'
		// 		})

		// 		fetch('http://localhost:4000/users/getUserDetails', {
		// 			headers: {
		// 				Authorization: `Bearer ${data.accessToken}`
		// 			}
		// 		})
		// 		.then(res=>res.json())
		// 		.then(result=>{
		// 			if(result.isAdmin === true){
		// 				localStorage.setItem('email', result.email);
		// 				localStorage.setItem('isAdmin', result.isAdmin);
		// 				setUser({
		// 					email: result.email,
		// 					isAdmin: result.isAdmin

		// 				})

		// 				navigate('/courses')
		// 			} else{
		// 				navigate('/')
		// 			}
		// 		})

		// 	}else{

		// 		Swal.fire({
		// 		  title: 'Oppss!',
		// 		  text: `Something went wrong. Check your credentials.!`,
		// 		  icon:'error'
		// 		})
		// 	}
		// })	

		// setEmail('');
		// setPassword1('');
		// setPassword2('');
		// Swal.fire({
		//   title: 'Good job!',
		//   text: 'Registration successful!',
		//   icon:'success'
		// })
	}

	return (

		(user.accessToken !== null) ?

			<Navigate to="/" />
		:

			<Form className = "p-2" onSubmit={(e) => registerUser(e) }>
				<Form.Group>
					<h1 className = "text-center mb-2">Register</h1>
					<Form.Label>Email Address</Form.Label>
					<Form.Control 
						type="email"
						placeholder = "ex.: juan@email.com"
						required
						value={email}
						onChange={e => setEmail(e.target.value)}
					/>
					<Form.Text className = "text-muted">
						We will never share your email with anyone else.
					</Form.Text>
				</Form.Group>

				<Form.Group>
					<Form.Label>Password</Form.Label>
					<Form.Control 
						type="password"
						placeholder = "Enter password"
						required
						value={password1}
						onChange={e => setPassword1(e.target.value)}
					/>
					<Form.Text className = "text-muted">
						User combination of lowercase and uppercase texts, special characters and numbers.
					</Form.Text>
				</Form.Group>

				<Form.Group>
					<Form.Label>Password</Form.Label>
					<Form.Control 
						type="password"
						placeholder = "Verify password"
						required
						value={password2}
						onChange={e => setPassword2(e.target.value)}
					/>
				</Form.Group>

				{isActive ?
					<Button className="mt-3" variant="primary" type="submit" > Submit </Button>
					:
					<Button className="mt-3" variant="primary" type="submit" disabled> Submit </Button>
				}

			</Form>
	)
}
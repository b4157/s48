import './App.css';
import AppNavBar from'./components/AppNavbar';
import Home from'./pages/Home';
import Courses from'./pages/Courses';
import Register from './pages/Register'
import Login from './pages/Login'
import Logout from './pages/Logout'
import PageNotFound from './components/PageNotFound'
import React, {useState} from 'react';
import {UserProvider} from './UserContext';
import  {Container} from "react-bootstrap";

// for Routes
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';


function App() {

  const [user, setUser] = useState({
      accessToken: localStorage.getItem('accessToken'),
      email: localStorage.getItem('email'),
      isAdmin: localStorage.getItem('isAdmin') === 'true'
  })
  const unsetUser = () => {
      localStorage.clear()
  }

  return (
    <UserProvider value = {{user, setUser, unsetUser}}>
      <Router>
        <AppNavBar />
        <Container>
            <Routes>
              <Route path="/" element={<Home />} />
              <Route path='/courses' element={<Courses />} />
              <Route path='/register' element={<Register />} />
              <Route path='/login' element={<Login />} />
              <Route path='/logout' element={<Logout />} />
              <Route path='*' element={<PageNotFound />} />
            </Routes>
        </Container>   
      </Router>
    </UserProvider>

  );
}

export default App;
  
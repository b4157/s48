const coursesData=
[
	{
		id: "wdc001",
		name: "PHP - Laravel",
		description: "Lorem ipsum dolor sit amet consectetur adipisicing, elit. Iusto accusantium esse earum, laboriosam amet porro aperiam recusandae dolore unde ipsa? Vero molestias est similique nisi dolores dolorem, reiciendis hic perspiciatis.",
		price: 45000,
		onOffer: true
	},
	{
		id: "wdc002",
		name: "Python - Django",
		description: "Lorem ipsum dolor sit amet consectetur adipisicing, elit. Iusto accusantium esse earum, laboriosam amet porro aperiam recusandae dolore unde ipsa? Vero molestias est similique nisi dolores dolorem, reiciendis hic perspiciatis.",
		price: 50000,
		onOffer: true
	},
	{
		id: "wdc003",
		name: "Java - Springboot",
		description: "Lorem ipsum dolor sit amet consectetur adipisicing, elit. Iusto accusantium esse earum, laboriosam amet porro aperiam recusandae dolore unde ipsa? Vero molestias est similique nisi dolores dolorem, reiciendis hic perspiciatis.",
		price: 55000,
		onOffer: true
	}

]
	
export	default coursesData;